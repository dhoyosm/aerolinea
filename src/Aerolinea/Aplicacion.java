package Aerolinea;

import java.util.ArrayList;
import java.util.Scanner;

import Entidad.Avion;
import Entidad.Pasajero;
import Entidad.Reserva;
import Entidad.Vuelo;

public class Aplicacion {
	
	private static final int ANCHO_TABLA = 34;
	private static final int MAXIMO_MALETAS = 2;
	
	private static ArrayList<Pasajero> pasajeros = new ArrayList<>(); //Lista que contiene todos los pasajeros
	private static ArrayList<Avion> aviones = new ArrayList<>(); //Lista que contiene todos los aviones
	private static ArrayList<Vuelo> vuelos = new ArrayList<>(); //Lista que contiene todos los vuelos
	private static ArrayList<Reserva> reservas = new ArrayList<>(); //Lista que contiene todas las reservas
	private static Scanner teclado = new Scanner(System.in);
	private static String mensajes = new String();

	public static void main(String[] args) {
		datosIniciales(); //Genera Datos iniciales
		menuPrincipal(); //Imprime el menu principal
	}
	
	public static void datosIniciales(){ //Genera datos iniciales
		// Crea 2 aviones y los almacena en la lista de aviones
		Avion avion1 = new Avion("01");
		Avion avion2 = new Avion("02");
		aviones.add(avion1);
		aviones.add(avion2);
		// Crea 2 pasajeros y los almacena en la lista de pasajeros
		Pasajero pasajero1 = new Pasajero("JOHN DOE", "KRA 1 NO 2 - 3", "(1)211-1111", "80123456", "(300)622-2222", "ANITA DOE");
		Pasajero pasajero2 = new Pasajero("SIMON EL BOBITO", "CALLE DEL CARTUCHO 123", "(1)666-6666", "110123456", "(300)666-6666", "EL NERO PINEDA");
		pasajeros.add(pasajero1);
		pasajeros.add(pasajero2);
		// Crea 2 vuelos, le asigna aviones, y lo almacena en la lista de vuelos
		Vuelo vuelo1 = new  Vuelo("VU001", "05/06/2013", "BOGOTA", "CARTAGENA", avion1);
		Vuelo vuelo2 = new  Vuelo("VU002", "06/06/2013", "BOGOTA", "CARTAGENA", avion1);
		Vuelo vuelo3 = new  Vuelo("VU003", "05/06/2013", "BOGOTA", "MEDELLIN", avion2);
		vuelos.add(vuelo1);
		vuelos.add(vuelo2);
		vuelos.add(vuelo3);
	}

	public static void menuPrincipal() {//Imprime el menu principal
		String opcion = new String();
		teclado = new Scanner(System.in);
		do {
			limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
			imprimirTitulo(); //Dibuja el titulo
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			System.out.println("|" + llenarDerecha("Que desea hacer?", " ", ANCHO_TABLA) + "|");
			System.out.println("+--+" + lineasHorizontales(ANCHO_TABLA-3) + "+");
			System.out.println("| S|"+llenarDerecha("SALIR", " ", ANCHO_TABLA-3)+"|");
			System.out.println("| 1|"+llenarDerecha("Pasajeros", " ", ANCHO_TABLA-3)+"|");
			System.out.println("| 2|"+llenarDerecha("Aviones", " ", ANCHO_TABLA-3)+"|");
			System.out.println("| 3|"+llenarDerecha("Vuelos", " ", ANCHO_TABLA-3)+"|");
			System.out.println("| 4|"+llenarDerecha("Reservas", " ", ANCHO_TABLA-3)+"|");
			System.out.println("| 5|"+llenarDerecha("Historial Reservas", " ", ANCHO_TABLA-3)+"|");
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			imprimirMensajes(); //Imprime mensajes del sistema
			subirPantalla();  //Sube el contenido aplicando espacios
			opcion = teclado.nextLine();
			opcion = opcion.toUpperCase(); //Pasa la opcion a mayuscula para comparar todo en mayuscula
			switch (opcion) {
			case "S":
				System.out.println("Salida correcta del sistema");
				break;
			case "1":
				menuPasajeros(); //Imprime el menu con el listado de pasajeros y opciones
				break;
			case "2":
				menuAviones(); //Imprime el menu con el listado de aviones y opciones
				break;
			case "3":
				menuVuelos(); //Imprime el menu con el listado de vuelos y opciones
				break;
			case "4":
				menuReservas(); //Imprime el menu con el listado de reservas y opciones
				break;
			case "5":
				historialReservas(); //Imprime el las reservas de un pasajero seleccionado
				break;
			default:
				mensajes += "ERROR - Opcion invalida";
				break;
			}
		} while (!opcion.equals("S"));
	}
	
	public static void menuPasajeros(){ //Imprime el menu con el listado de pasajeros y opciones
		String opcion = new String();
		teclado = new Scanner(System.in);
		String cad = new String();
		do {
			limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
			imprimirTitulo(); //Dibuja el titulo
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			System.out.println("|" + llenarDerecha("N - Nuevo pasajero", " ", ANCHO_TABLA) + "|");
			System.out.println("|" + llenarDerecha("S - Volver al menu", " ", ANCHO_TABLA) + "|");
			String lineasHorizontales = "+--+" + lineasHorizontales(31) + "+" + lineasHorizontales(25) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(30) + "+" ;
			System.out.println(lineasHorizontales);
			cad = new String();
			cad += "|  ";
			cad += "|" + llenarDerecha(" Nombre"," ", 31) ;
			cad += "|" + llenarDerecha(" Direccion"," ", 25) ;
			cad += "|" + llenarDerecha(" Telefono"," ", 15) ;
			cad += "|" + llenarDerecha(" Pasaporte"," ", 15) ;
			cad += "|" + llenarDerecha(" Celular"," ", 15) ;
			cad += "|" + llenarDerecha(" Contacto"," ", 30) ;
			cad += "|";
			System.out.println(cad);
			System.out.println(lineasHorizontales);
			for (int i = 0; i<pasajeros.size(); i++){ //Imprime el listado de pasajeros 
				Pasajero pasajero = pasajeros.get(i);
				cad = new String();
				cad += "|" + llenarIzquierda(String.valueOf(i)," ", 2);
				cad += "|" + llenarDerecha(pasajero.getNombre()," ", 31);
				cad += "|" + llenarDerecha(pasajero.getDireccion()," ", 25);
				cad += "|" + llenarIzquierda(pasajero.getTelefono()," ", 15);
				cad += "|" + llenarIzquierda(pasajero.getPasaporte()," ", 15);
				cad += "|" + llenarIzquierda(pasajero.getCelular()," ", 15);
				cad += "|" + llenarDerecha(pasajero.getNombreContacto()," ", 30);
				cad += "|";
				System.out.println(cad);
			}
			System.out.println(lineasHorizontales);
			imprimirMensajes(); //Imprime mensajes del sistema
			subirPantalla(); //Sube el contenido aplicando espacios
			opcion = teclado.nextLine();
			opcion = opcion.toUpperCase(); //Pasa la opcion a mayuscula para comparar todo en mayuscula
			switch (opcion) {
			case "N":
				crearPasajero(); //Proceso para crear pasajero
				break;
			case "S":
				break;
			default:
				mensajes += "ERROR - Opcion invalida";
				break;
			}
		} while (!opcion.equals("S"));
	}
	
	public static void menuAviones(){ //Imprime el menu con el listado de aviones y opciones
		String opcion = new String();
		teclado = new Scanner(System.in);
		do {
			limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
			imprimirTitulo(); //Dibuja el titulo
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			System.out.println("|" + llenarDerecha("N - Nuevo avion", " ", ANCHO_TABLA) + "|");
			System.out.println("|" + llenarDerecha("S - Volver al menu", " ", ANCHO_TABLA) + "|");
			System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
			System.out.println("|  |" + llenarDerecha(" Id."," ", 15) + "|" + llenarDerecha(" No. Sillas"," ", 15) + "|");
			System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
			for (int i = 0; i<aviones.size(); i++){ //Imprime el listado de aviones 
				Avion avion = aviones.get(i);
				String cad = new String();
				cad += "|" + llenarIzquierda(String.valueOf(i)," ", 2);
				cad += "|" + llenarIzquierda(avion.getId()," ", 15);
				cad += "|" + llenarIzquierda(String.valueOf(avion.getNumeroSillas())," ", 15);
				cad += "|";
				System.out.println(cad);
			}
			System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
			imprimirMensajes(); //Imprime mensajes del sistema
			subirPantalla(); //Sube el contenido aplicando espacios
			opcion = teclado.nextLine();
			opcion = opcion.toUpperCase(); //Pasa la opcion a mayuscula para comparar todo en mayuscula
			switch (opcion) {
			case "N":
				crearAvion(); //Crea un avion automaticamente
				break;
			case "S":
				break;
			default:
				mensajes += "ERROR - Opcion invalida";
				break;
			}
		} while (!opcion.equals("S"));
	}
	
	public static void menuVuelos(){ //Imprime el menu con el listado de vuelos y opciones
		String opcion = new String();
		teclado = new Scanner(System.in);
		String cad = new String();
		do {
			limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
			imprimirTitulo(); //Dibuja el titulo
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			System.out.println("|" + llenarDerecha("N - Nuevo vuelo", " ", ANCHO_TABLA) + "|");
			System.out.println("|" + llenarDerecha("S - Volver al menu", " ", ANCHO_TABLA) + "|");
			String lineasHorizontales = "+---+" + lineasHorizontales(5) + "+" + lineasHorizontales(10) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(6) + "+" + lineasHorizontales(7) + "+" + lineasHorizontales(7) + "+";
			System.out.println(lineasHorizontales);
			cad = new String();
			cad += "|   ";
			cad += "|" + llenarDerecha(" Cod."," ", 5) ;
			cad += "|" + llenarDerecha(" Fecha"," ", 10) ;
			cad += "|" + llenarDerecha(" Origen"," ", 20) ;
			cad += "|" + llenarDerecha(" Destino"," ", 20) ;
			cad += "|" + llenarDerecha(" Avion"," ", 6) ;
			cad += "|" + llenarDerecha(" Sillas"," ", 7) ;
			cad += "|" + llenarDerecha(" Estado"," ", 7) ;
			cad += "|";
			System.out.println(cad);
			System.out.println(lineasHorizontales);
			for (int i = 0; i<vuelos.size(); i++){ //Imprime el listado de pasajeros 
				Vuelo vuelo = vuelos.get(i);
				cad = new String();
				cad += "|" + llenarIzquierda(String.valueOf(i)," ", 3);
				cad += "|" + llenarDerecha(vuelo.getCodigo()," ", 5);
				cad += "|" + llenarDerecha(vuelo.getFechaVuelo()," ", 10);
				cad += "|" + llenarDerecha(vuelo.getCiudadOrigen()," ", 20);
				cad += "|" + llenarDerecha(vuelo.getCiudadDestino()," ", 20);
				cad += "|" + llenarIzquierda(vuelo.getAvion().getId()," ", 6);
				cad += "|" + llenarIzquierda(String.valueOf(vuelo.getSillasDisponibles())," ", 7);
				String estado = "ABIERTO";
				if (vuelo.isVueloCerrado()==true){
					estado = "CERRADO";
				}
				cad += "|" + llenarIzquierda(estado," ", 7);
				cad += "|";
				System.out.println(cad);
			}
			System.out.println(lineasHorizontales);
			imprimirMensajes(); //Imprime mensajes del sistema
			subirPantalla(); //Sube el contenido aplicando espacios
			opcion = teclado.nextLine();
			opcion = opcion.toUpperCase(); //Pasa la opcion a mayuscula para comparar todo en mayuscula
			switch (opcion) {
			case "N":
				crearVuelo(); //Proceso para crear un vuelo
				break;
			case "S":
				break;
			default:
				mensajes += "ERROR - Opcion invalida";
				break;
			}
		} while (!opcion.equals("S"));
	}
	
	public static void menuReservas(){ //Imprime el menu con el listado de reservas y opciones
		String opcion = new String();
		teclado = new Scanner(System.in);
		String cad = new String();
		do {
			limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
			imprimirTitulo(); //Dibuja el titulo
			System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
			System.out.println("|" + llenarDerecha("N - Nueva reserva", " ", ANCHO_TABLA) + "|");
			System.out.println("|" + llenarDerecha("C - Cancelar reserva", " ", ANCHO_TABLA) + "|");
			System.out.println("|" + llenarDerecha("S - Volver al menu", " ", ANCHO_TABLA) + "|");
			String lineasHorizontales = "+---+" + lineasHorizontales(30) + "+" + lineasHorizontales(10) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(6) + "+" + lineasHorizontales(9) + "+";
			System.out.println(lineasHorizontales);
			cad = new String();
			cad += "|   ";
			cad += "|" + llenarDerecha(" Pasajero"," ", 30) ;
			cad += "|" + llenarDerecha(" Fecha"," ", 10) ;
			cad += "|" + llenarDerecha(" Origen"," ", 20) ;
			cad += "|" + llenarDerecha(" Destino"," ", 20) ;
			cad += "|" + llenarDerecha(" Maletas"," ", 6) ;
			cad += "|" + llenarDerecha(" Estado"," ", 9) ;
			cad += "|";
			System.out.println(cad);
			System.out.println(lineasHorizontales);
			for (int i = 0; i<reservas.size(); i++){ //Imprime el listado de reservas 
				Reserva reserva = reservas.get(i);
				cad = new String();
				cad += "|" + llenarIzquierda(String.valueOf(i)," ", 3);
				cad += "|" + llenarDerecha(reserva.getPasajero().getNombre()," ", 30);
				cad += "|" + llenarDerecha(reserva.getVuelo().getFechaVuelo()," ", 10);
				cad += "|" + llenarDerecha(reserva.getVuelo().getCiudadOrigen()," ", 20);
				cad += "|" + llenarDerecha(reserva.getVuelo().getCiudadDestino()," ", 20);
				cad += "|" + llenarIzquierda(String.valueOf(reserva.getNumeroMaletas())," ", 6);
				cad += "|" + llenarIzquierda(reserva.getEstado()," ", 9);
				cad += "|";
				System.out.println(cad);
			}
			System.out.println(lineasHorizontales);
			imprimirMensajes(); //Imprime mensajes del sistema
			subirPantalla(); //Sube el contenido aplicando espacios
			opcion = teclado.nextLine();
			opcion = opcion.toUpperCase(); //Pasa la opcion a mayuscula para comparar todo en mayuscula
			switch (opcion) {
			case "N":
				crearReserva(); //Proceso para crear una reserva
				break;
			case "C":
				cancelarReserva(); //Proceso para cancelar una reserva
				break;
			case "S":
				break;
			default:
				mensajes += "ERROR - Opcion invalida";
				break;
			}
		} while (!opcion.equals("S"));
	}
	
	public static void crearPasajero(){  //Proceso para crear pasajero
		Pasajero nuevoPasajero = new Pasajero();
		System.out.println("Nombre del Pasajero: ");
		nuevoPasajero.setNombre(teclado.nextLine().toUpperCase()); //Todo en mayusculas
		System.out.println("Direccion del Pasajero: ");
		nuevoPasajero.setDireccion(teclado.nextLine().toUpperCase()); //Todo en mayusculas
		System.out.println("Telefono del Pasajero: ");
		nuevoPasajero.setTelefono(teclado.nextLine());
		System.out.println("Pasaporte del Pasajero: ");
		nuevoPasajero.setPasaporte(teclado.nextLine());
		System.out.println("Celular del Pasajero: ");
		nuevoPasajero.setCelular(teclado.nextLine());
		System.out.println("Nombre de Contacto: ");
		nuevoPasajero.setNombreContacto(teclado.nextLine().toUpperCase()); //Todo en mayusculas
		pasajeros.add(nuevoPasajero);
	}
	
	public static void crearAvion(){ //Crea un avion automaticamente con un id consecutivo
		Avion avion = new Avion(llenarIzquierda(String.valueOf(aviones.size() + 1), "0", 2));
		aviones.add(avion);
	}
	
	public static void crearVuelo(){
		Vuelo vuelo = new Vuelo();
		vuelo.setCodigo("VU"+llenarIzquierda(String.valueOf(vuelos.size()+1), "0", 3));
		System.out.println("Ciudad Origen: ");
		vuelo.setCiudadOrigen(teclado.nextLine().toUpperCase()); //Todo en mayusculas
		System.out.println("Ciudad Destino: ");
		vuelo.setCiudadDestino(teclado.nextLine().toUpperCase()); //Todo en mayusculas
		System.out.println("Fecha (DD/MM/AAAA): ");
		vuelo.setFechaVuelo(teclado.nextLine());
		System.out.println("Asignar Avion: ");
		System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
		System.out.println("|  |" + llenarDerecha(" Id."," ", 15) + "|" + llenarDerecha(" No. Sillas"," ", 15) + "|");
		System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
		for (int i = 0; i<aviones.size(); i++){ //Imprime el listado de aviones 
			Avion avion = aviones.get(i);
			String cad = new String();
			cad += "|" + llenarIzquierda(String.valueOf(i)," ", 2);
			cad += "|" + llenarIzquierda(avion.getId()," ", 15);
			cad += "|" + llenarIzquierda(String.valueOf(avion.getNumeroSillas())," ", 15);
			cad += "|";
			System.out.println(cad);
		}
		System.out.println("+--+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+");
		Avion avion = null;
		do{
			try{ // intente lo siguiente mientras que no exista un avion
				System.out.println("Seleccione un Avion: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i>=0 && i<aviones.size()){
					avion = aviones.get(i);
				}else{
					System.out.println("Opcion incorrecta");
				}
				
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(avion==null);
		vuelo.setAvion(avion);
		vuelo.setSillasDisponibles(avion.getNumeroSillas()); //asigna el numero de sillas de avion al numero de sillas disponibles para el vuelo
		imprimirMensajes(); //Imprime mensajes del sistema
		vuelos.add(vuelo);
		teclado = new Scanner(System.in);
	}
	
	public static void crearReserva(){
		Reserva reserva = new Reserva();
		String cad = new String();
		System.out.println("Pasajero: ");
		String lineasHorizontales = "+--+" + lineasHorizontales(31) + "+" + lineasHorizontales(25) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(30) + "+";
		System.out.println(lineasHorizontales);
		cad = new String();
		cad += "|  ";
		cad += "|" + llenarDerecha(" Nombre"," ", 31) ;
		cad += "|" + llenarDerecha(" Direccion"," ", 25) ;
		cad += "|" + llenarDerecha(" Telefono"," ", 15) ;
		cad += "|" + llenarDerecha(" Pasaporte"," ", 15) ;
		cad += "|" + llenarDerecha(" Celular"," ", 15) ;
		cad += "|" + llenarDerecha(" Contacto"," ", 30) ;
		System.out.println(cad);
		System.out.println(lineasHorizontales);
		for (int i = 0; i<pasajeros.size(); i++){ //Imprime el listado de pasajeros 
			Pasajero pasajero = pasajeros.get(i);
			cad = new String();
			cad += "|" + llenarIzquierda(String.valueOf(i)," ", 2);
			cad += "|" + llenarDerecha(pasajero.getNombre()," ", 31);
			cad += "|" + llenarDerecha(pasajero.getDireccion()," ", 25);
			cad += "|" + llenarIzquierda(pasajero.getTelefono()," ", 15);
			cad += "|" + llenarIzquierda(pasajero.getPasaporte()," ", 15);
			cad += "|" + llenarIzquierda(pasajero.getCelular()," ", 15);
			cad += "|" + llenarDerecha(pasajero.getNombreContacto()," ", 30);
			cad += "|";
			System.out.println(cad);
		}
		System.out.println(lineasHorizontales);
		Pasajero pasajero = null;
		do{
			try{ // intente lo siguiente mientras que no exista un pasajero
				System.out.println("Seleccione un Pasajero: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i>=0 && i<pasajeros.size()){
					pasajero = pasajeros.get(i);
				}else{
					System.out.println("Opcion incorrecta");
				}
				
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(pasajero==null);
		reserva.setPasajero(pasajero);
		System.out.println("Pasajero: "+pasajero.getNombre());
		System.out.println("Vuelo: ");
		lineasHorizontales = new String();
		lineasHorizontales = "+---+" + lineasHorizontales(5) + "+" + lineasHorizontales(10) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(6) + "+" + lineasHorizontales(7) + "+" + lineasHorizontales(7) + "+";
		System.out.println(lineasHorizontales);
		cad = new String();
		cad += "|   ";
		cad += "|" + llenarDerecha(" Cod."," ", 5) ;
		cad += "|" + llenarDerecha(" Fecha"," ", 10) ;
		cad += "|" + llenarDerecha(" Origen"," ", 20) ;
		cad += "|" + llenarDerecha(" Destino"," ", 20) ;
		cad += "|" + llenarDerecha(" Avion"," ", 6) ;
		cad += "|" + llenarDerecha(" Sillas"," ", 7) ;
		cad += "|" + llenarDerecha(" Estado"," ", 7) ;
		cad += "|";
		System.out.println(cad);
		System.out.println(lineasHorizontales);
		for (int i = 0; i<vuelos.size(); i++){ //Imprime el listado de pasajeros 
			Vuelo vuelo = vuelos.get(i);
			cad = new String();
			cad += "|" + llenarIzquierda(String.valueOf(i)," ", 3);
			cad += "|" + llenarDerecha(vuelo.getCodigo()," ", 5);
			cad += "|" + llenarDerecha(vuelo.getFechaVuelo()," ", 10);
			cad += "|" + llenarDerecha(vuelo.getCiudadOrigen()," ", 20);
			cad += "|" + llenarDerecha(vuelo.getCiudadDestino()," ", 20);
			cad += "|" + llenarIzquierda(vuelo.getAvion().getId()," ", 6);
			cad += "|" + llenarIzquierda(String.valueOf(vuelo.getSillasDisponibles())," ", 7);
			String estado = "ABIERTO";
			if (vuelo.isVueloCerrado()==true){
				estado = "CERRADO";
			}
			cad += "|" + llenarIzquierda(estado," ", 7);
			cad += "|";
			System.out.println(cad);
		}
		System.out.println(lineasHorizontales);
		Vuelo vuelo = null;
		do{
			try{ // intente lo siguiente mientras que no exista un vuelo
				System.out.println("Seleccione un Vuelo: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i>=0 && i<vuelos.size()){
					vuelo = vuelos.get(i);
					if(vuelo.isVueloCerrado() == true){ //Valida si el vuelo esta cerrado para no seleccionarlo en caso dado
						System.out.println("el vuelo "+vuelo.getCodigo()+" esta cerrado. Seleccione otro vuelo.");
						vuelo = null;
					} else{
						int sillasDisponibles = vuelo.getSillasDisponibles();
						sillasDisponibles = sillasDisponibles - 1;
						vuelo.setSillasDisponibles(sillasDisponibles);
						if (sillasDisponibles == 0){
							vuelo.setVueloCerrado(true);
						}
						vuelos.set(i, vuelo); //Actualiza la informacion del vuelo
					}
				}else{
					System.out.println("Opcion incorrecta");
				}
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(vuelo==null);
		reserva.setVuelo(vuelo);
		System.out.println("Vuelo: "+vuelo.getCodigo());
		int maletas = -1;
		do{
			try{ // intente lo siguiente mientras que el numero de maletas sea valido
				System.out.println("Numero de Maletas: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i<0){
					System.out.println("Numero de maletas debe ser mayor o igual a 0");
				} else if(i>MAXIMO_MALETAS){
					System.out.println("Se permite un maximo de "+MAXIMO_MALETAS+" maletas");
				} else {
					maletas = i;
				}
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(maletas==-1);
		reserva.setNumeroMaletas(maletas);
		reserva.setEstado("ACTIVO");
		reservas.add(reserva);
		teclado = new Scanner(System.in);
	}
	
	public static void cancelarReserva(){
		Reserva reserva = null;
		do{
			try{ // intente lo siguiente mientras que no exista una reserva
				System.out.println("Seleccione una Reserva a Cancelar: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i>=0 && i<reservas.size()){
					reserva = reservas.get(i);
					reserva.setEstado("CANCELADO");
					reservas.set(i, reserva); //Actualiza la informacion de la reserva
					Vuelo vuelo = reserva.getVuelo();
					int j=vuelos.indexOf(vuelo); //Obtiene el inidice del vuelo
					int sillasDisponibles = vuelo.getSillasDisponibles();
					sillasDisponibles = sillasDisponibles + 1;
					vuelo.setSillasDisponibles(sillasDisponibles);
					if (sillasDisponibles > 0){
						vuelo.setVueloCerrado(false);
					}
					vuelos.set(j, vuelo); //Actualiza la informacion del vuelo
				}else{
					System.out.println("Opcion incorrecta");
				}
				
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(reserva==null);
		teclado = new Scanner(System.in);
	}
	
	public static void historialReservas(){ //Imprime las reservas de un pasajero seleccionado
		String cad = new String();
		System.out.println("Pasajero: ");
		String lineasHorizontales = "+--+" + lineasHorizontales(31) + "+" + lineasHorizontales(25) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(15) + "+" + lineasHorizontales(30) + "+";
		System.out.println(lineasHorizontales);
		cad = new String();
		cad += "|  ";
		cad += "|" + llenarDerecha(" Nombre"," ", 31) ;
		cad += "|" + llenarDerecha(" Direccion"," ", 25) ;
		cad += "|" + llenarDerecha(" Telefono"," ", 15) ;
		cad += "|" + llenarDerecha(" Pasaporte"," ", 15) ;
		cad += "|" + llenarDerecha(" Celular"," ", 15) ;
		cad += "|" + llenarDerecha(" Contacto"," ", 30) ;
		System.out.println(cad);
		System.out.println(lineasHorizontales);
		for (int i = 0; i<pasajeros.size(); i++){ //Imprime el listado de pasajeros 
			Pasajero pasajero = pasajeros.get(i);
			cad = new String();
			cad += "|" + llenarIzquierda(String.valueOf(i)," ", 2);
			cad += "|" + llenarDerecha(pasajero.getNombre()," ", 31);
			cad += "|" + llenarDerecha(pasajero.getDireccion()," ", 25);
			cad += "|" + llenarIzquierda(pasajero.getTelefono()," ", 15);
			cad += "|" + llenarIzquierda(pasajero.getPasaporte()," ", 15);
			cad += "|" + llenarIzquierda(pasajero.getCelular()," ", 15);
			cad += "|" + llenarDerecha(pasajero.getNombreContacto()," ", 30);
			cad += "|";
			System.out.println(cad);
		}
		System.out.println(lineasHorizontales);
		Pasajero pasajero = null;
		do{
			try{ // intente lo siguiente mientras que no exista un pasajero
				System.out.println("Seleccione un Pasajero: ");
				teclado = new Scanner(System.in);
				int i=teclado.nextInt(); //Es probable que arroje excepcion si no es un entero
				if(i>=0 && i<pasajeros.size()){
					pasajero = pasajeros.get(i);
				}else{
					System.out.println("Opcion incorrecta");
				}
				
			}catch(Exception e){ //atrape la excepcion del sistema si existe
				System.out.println("Opcion incorrecta");
			}
		}while(pasajero==null);
		limpiarPantalla(); //Limpia el contenido de la pantalla imprimiendo muchos espacios
		imprimirTitulo(); //Dibuja el titulo
		System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
		System.out.println("|" + llenarDerecha("Historial Pasajero:", " ", ANCHO_TABLA) + "|");
		System.out.println("|" + llenarDerecha(pasajero.getNombre(), " ", ANCHO_TABLA) + "|");
		lineasHorizontales = "+---+" + lineasHorizontales(10) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(20) + "+" + lineasHorizontales(6) + "+" + lineasHorizontales(9) + "+";
		System.out.println(lineasHorizontales);
		cad = new String();
		cad += "|   ";
		cad += "|" + llenarDerecha(" Fecha"," ", 10) ;
		cad += "|" + llenarDerecha(" Origen"," ", 20) ;
		cad += "|" + llenarDerecha(" Destino"," ", 20) ;
		cad += "|" + llenarDerecha(" Maletas"," ", 6) ;
		cad += "|" + llenarDerecha(" Estado"," ", 9) ;
		cad += "|";
		System.out.println(cad);
		System.out.println(lineasHorizontales);
		for (int i = 0; i<reservas.size(); i++){ //Imprime el listado de reservas 
			Reserva reserva = reservas.get(i);
			if(pasajero.equals(reserva.getPasajero())){
				cad = new String();
				cad += "|" + llenarIzquierda(String.valueOf(i)," ", 3);
				cad += "|" + llenarDerecha(reserva.getVuelo().getFechaVuelo()," ", 10);
				cad += "|" + llenarDerecha(reserva.getVuelo().getCiudadOrigen()," ", 20);
				cad += "|" + llenarDerecha(reserva.getVuelo().getCiudadDestino()," ", 20);
				cad += "|" + llenarIzquierda(String.valueOf(reserva.getNumeroMaletas())," ", 6);
				cad += "|" + llenarIzquierda(reserva.getEstado()," ", 9);
				cad += "|";
				System.out.println(cad);
			}
		}
		System.out.println(lineasHorizontales);
		subirPantalla(); //Sube el contenido aplicando espacios
		System.out.println("Oprima cualquier tecla para volver al menu principal");
		teclado = new Scanner(System.in);
		teclado.nextLine();
	}
	
	public static void limpiarPantalla(){ //Limpia el contenido de la pantalla imprimiendo muchos saltos de linea
		for(int i=0; i<512; i++){
			System.out.println("");
		}
	}
	
	public static void imprimirTitulo(){ //Dibuja el titulo
		System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
		System.out.println("|+" + lineasHorizontales(ANCHO_TABLA-2) + "+|");
		System.out.println("|| Bienvenido a 'Aerolinea (V.3)' ||");		
		System.out.println("|+" + lineasHorizontales(ANCHO_TABLA-2) + "+|");
		System.out.println("+" + lineasHorizontales(ANCHO_TABLA) + "+");
		System.out.println("");
	}
	
	public static void subirPantalla(){ //Sube el contenido aplicando espacios
		for(int i=0; i<10; i++){
			System.out.println("");
		}
	}
	
	public static void imprimirMensajes(){ //Imprime mensajes del sistema
		System.out.println(mensajes);
		mensajes = new String();
	}
	
	public static String lineasHorizontales(int n){ //Dibuja n lineas horizontales
		String cad = new String();
		for(int i = 0; i < n; i++){
			cad += "-";
		}
		return cad;
	}
	
	public static String llenarDerecha(String cadena, String car, int tamano){ //Llena la derecha de una cadena con caracteres
		if(cadena.length() > tamano){
			return cadena.substring(0, tamano); // Corta la cadena al tamano si esta es mayor
		} else{
			for(int i=cadena.length(); i<tamano; i++){
				cadena += car;
			}
		}
		return cadena;
	}
	
	public static String llenarIzquierda(String cadena, String car, int tamano){ //Llena la izquierda de una cadena con caracteres
		if(cadena.length() > tamano){
			return cadena.substring(0, tamano); // Corta la cadena al tamano si esta es mayor
		} else{
			for(int i=cadena.length(); i<tamano; i++){
				cadena = car + cadena;
			}
		}
		return cadena;
	}
}
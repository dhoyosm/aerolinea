package Entidad;

public class Vuelo 
{

	private String codigo;
	private String fechaVuelo;
	private String ciudadOrigen;
	private String ciudadDestino;
	private Avion avion;
	private int sillasDisponibles;
	private boolean vueloCerrado;
	
	public Vuelo(){
		
	}
	
	public Vuelo(String codigo, String fechaVuelo, String ciudadOrigen,
			String ciudadDestino, Avion avion) {
		this.codigo = codigo;
		this.fechaVuelo = fechaVuelo;
		this.ciudadOrigen = ciudadOrigen;
		this.ciudadDestino = ciudadDestino;
		this.avion = avion;
		this.sillasDisponibles = avion.getNumeroSillas();
		this.vueloCerrado = false;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getFechaVuelo() {
		return fechaVuelo;
	}
	public void setFechaVuelo(String fechaVuelo) {
		this.fechaVuelo = fechaVuelo;
	}
	public String getCiudadOrigen() {
		return ciudadOrigen;
	}
	public void setCiudadOrigen(String ciudadOrigen) {
		this.ciudadOrigen = ciudadOrigen;
	}
	public String getCiudadDestino() {
		return ciudadDestino;
	}
	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}
	public Avion getAvion() {
		return avion;
	}
	public void setAvion(Avion avion) {
		this.avion = avion;
	}
	public int getSillasDisponibles() {
		return sillasDisponibles;
	}
	public void setSillasDisponibles(int sillasDisponibles) {
		this.sillasDisponibles = sillasDisponibles;
	}
	public boolean isVueloCerrado() {
		return vueloCerrado;
	}
	public void setVueloCerrado(boolean vueloCerrado) {
		this.vueloCerrado = vueloCerrado;
	}
	
	
		
	
}

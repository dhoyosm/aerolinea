package Entidad;

public class Avion 
{
	private String id;
	
	private int numeroSillas;

	public Avion(){
		
	}
	
	public Avion(String id){
		this.id = "AV" + id;
		this.numeroSillas = 10;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getNumeroSillas() {
		return numeroSillas;
	}

	public void setNumeroSillas(int numeroSillas) {
		this.numeroSillas = numeroSillas;
	}
}
package Entidad;

public class Pasajero {
	private String nombre;

	private String direccion;

	private String telefono;

	private String pasaporte;

	private String celular;

	private String nombreContacto;
	
	public Pasajero() {
	}
	
	public Pasajero(String nombre, String direccion, String telefono,
			String pasaporte, String celular, String nombreContacto) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.pasaporte = pasaporte;
		this.celular = celular;
		this.nombreContacto = nombreContacto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPasaporte() {
		return pasaporte;
	}

	public void setPasaporte(String pasaporte) {
		this.pasaporte = pasaporte;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

}

package Entidad;

public class Reserva {
	
	private Pasajero pasajero;
	private Vuelo vuelo;
	private int numeroMaletas;
	private String estado;
	
	public Pasajero getPasajero() {
		return pasajero;
	}
	public void setPasajero(Pasajero pasajero) {
		this.pasajero = pasajero;
	}
	public Vuelo getVuelo() {
		return vuelo;
	}
	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}
	public int getNumeroMaletas() {
		return numeroMaletas;
	}
	public void setNumeroMaletas(int numeroMaletas) {
		this.numeroMaletas = numeroMaletas;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}

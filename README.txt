Aerolínea (V.3)
----------------------------------------------------------------------------------
Como tal, un sistema de reservas de una aerolínea funciona de la siguiente manera:
• Las personas o clientes se deben registrar con los datos mínimos (nombre, 
  dirección, teléfono, pasaporte, celular, nombre de contacto).
• Todos los aviones tienen una capacidad máxima de 10 puestos.
• Los vuelos tienen un código que lo identifica, una fecha para el viaje, una 
  ciudad origen y una ciudad destino.
• A un vuelo se le puede asignar máximo un avión (o no tener asignado avión aún).
• Cada vuelo o trayecto tiene hasta 10 sillas disponibles (por el avión). Si se
  llena un vuelo se debe considerar como vuelo cerrado.
• Cuando un cliente se registra en un vuelo, puede registrar hasta máximo dos
  maletas.
• Un cliente podía reservar un vuelo o trayecto de los disponibles.

Se necesita que en el programa:
• Se puedan registrar la información de los clientes, las reservas, los vuelos,
  los aviones y las maletas.
• Se puedan hacer reservas y cancelaciones de las mismas, con las reglas
  anteriormente mencionadas.
• Se debe poder conocer todo el inventario actual de vuelos y de aviones, tanto
  los asignados como los que no.
• Debe poderse ver un histórico de reservas y vuelos por cada cliente.
----------------------------------------------------------------------------------